// Considerar mover la funcionalidad de tienda a su propia clase a fin de clarificar y facilitar el mantenimiento del código
var tienda = null;

function obtenerDisponibilidad()
{    
    var url = "http://hispabyte.net/DWEC/entregable2-3.php";
    var procesaError = null;
    var metodo = "POST";
    var parametros = null;
    var contentType = "application/x-www-form-urlencoded";
    var procesaCaducidad = null;
    var caducidad = null;
    var cargador = new miAjax.CargadorContenidos(
                url,
                procesaRespuesta,
                procesaError,
                procesaCarga,
                metodo,
                parametros,
                contentType,
                procesaCaducidad,
                caducidad);
}

function procesaCarga()
{
    document.getElementById("articulosLista").innerHTML = "Cargando...";
}

function procesaRespuesta()
{
    if (this.peticion.readyState == miAjax.READY_STATE_COMPLETE && this.peticion.status == 200)
    {
        var respuesta_json = this.peticion.responseText;
        var respuesta = JSON.parse(respuesta_json);
                
        localStorage.setItem("tienda", JSON.stringify(respuesta));
        
        tienda = new Tienda(respuesta);
    }
}

window.onload = function()
{
    var tienda = JSON.parse(localStorage.getItem("tienda"));
    var estadoTienda = document.getElementById("estadoTienda");
            
    if (tienda == null)
    {
        obtenerDisponibilidad();
        
        estadoTienda.innerHTML = "Nueva tienda";
    }
    else
    {
        tienda = new Tienda(tienda);
        
        estadoTienda.innerHTML = "Tienda existente";
    }
    
    var botonRecargarTienda = document.getElementById("botonRecargarTienda");
    botonRecargarTienda.onclick = tienda.recargarTienda;
}
