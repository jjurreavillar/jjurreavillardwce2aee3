function Carrito(datos)
{
    carro = {};
    this.datos = datos;
    
    function actualizarCarrito()
    {    
        var elemCarrito = document.getElementById("carrito");

        elemCarrito.innerHTML = "";

        var valorTotal = 0;

        for (var i = 0; i < datos.length; i++)
        {            
            // Comprobar si la id actual existe como clave en el carrito
            if (!(datos[i].id in carro))
                continue;

            var tr = document.createElement("tr");

            // Primera celda
            var td = document.createElement("td");
            td.appendChild(document.createTextNode(datos[i].nombre));

            // Segunda celda
            var aSustraer = document.createElement("a");
            aSustraer.addEventListener("click", function() { eliminarDatoCarrito(this.id); });
            aSustraer.href = "#";
            aSustraer.id = datos[i].id;
            aSustraer.title = "-";
            aSustraer.appendChild(document.createTextNode("-"));

            var td2 = document.createElement("td");
            td2.style.width = "10px";
            td2.appendChild(aSustraer);

            // Tercera celda
            var aSumar = document.createElement("a");
            aSumar.addEventListener("click", function() { insertarDatoCarrito(this.id); });
            aSumar.href = "#";
            aSumar.id = datos[i].id;
            aSumar.title = "+";
            aSumar.appendChild(document.createTextNode("+"));

            var td3 = document.createElement("td");
            td3.style.width = "10px";
            td3.appendChild(aSumar);

            // Cuarta celda
            var td4 = document.createElement("td");
            td4.style.width = "10px";
            td4.appendChild(document.createTextNode(carro[datos[i].id]));

            tr.appendChild(td);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);

            elemCarrito.appendChild(tr);

            valorTotal += (datos[i].precio * carro[datos[i].id]);
        }

        if (!estaCarritoVacio())
        {
            var a = document.createElement("a");
            a.addEventListener("click", function() { vaciarCarrito(); });
            a.href = "#";
            a.title = "Vaciar carrito";

            var vaciar = document.createElement("label");
            vaciar.appendChild(document.createTextNode("Vaciar carrito"));
            vaciar.className = "etiquetas-datos";

            a.appendChild(vaciar);

            var total = document.createElement("label")
            total.appendChild(document.createTextNode("Total: " + valorTotal + " €"));
            total.className = "etiquetas-datos precio";

            elemCarrito.appendChild(a);
            elemCarrito.appendChild(total);
        }
    }
    
    this.actualizarCarrito = actualizarCarrito;
    
    function eliminarDatoCarrito(id)
    {
        carro[id]--;

        if (carro[id] < 1)
            delete carro[id];

        actualizarCarrito();
    }
    
    this.eliminarDatoCarrito = eliminarDatoCarrito;

    function insertarDatoCarrito(id)
    {        
        if (carro[id] == null)
            carro[id] = 1;
        else
            for (var i = 0; i < datos.length; i++)
            {
                if (datos[i].id != id)
                    continue;

                if (carro[id] < datos[i].unidades)
                    carro[id]++;

                break;
            }

        actualizarCarrito();
    }
    
    this.insertarDatoCarrito = insertarDatoCarrito;
    
    function insertarMultiples(id)
    {
        id = id.substr(5);
        
        var cantidadASumar = document.getElementById("cantidad" + id);
        
        // Si se ha introducido un valor no numérico, avisamos al usuario y cancelamos la operación
        if (!/^\d+$/.test(cantidadASumar.value))
        {
            alert("Error: " + cantidadASumar.value + " no es un número válido.");
            
            return;
        }
        
        for (var i = 0; i < cantidadASumar.value; i++)
            insertarDatoCarrito(id);
    }
    
    this.insertarMultiples = insertarMultiples;
    
    // Privada
    function estaCarritoVacio()
    {        
        for (var i in carro)
            return false;
        
        return true;
    }
        
    function vaciarCarrito()
    {
        carro = {};

        actualizarCarrito();
    }
    
    this.vaciarCarrito = vaciarCarrito;
}
    