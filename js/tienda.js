function Tienda(datos)
{
    this.datos = datos;
    carrito = new Carrito(datos);
    constructor(datos);
    
    function constructor(datos)
    {        
        construirCategorias(datos);

        /* Auto-inicializar a la primera categoría.
           Si la categoría indicada no existe o no es válida, no se construirá la lista de artículos */
        construirArticulos(datos, 1, carrito);
    }

    // Privado
    function construirArticulos(datos, categoria)
    {    
        var articulosTabla = document.getElementById("articulosTabla");

        // Vaciar la lista de artículos
        articulosTabla.innerHTML = "";

        for (var i = 0; i < datos.length; i++)
        {
            if (datos[i].cat != categoria)
                continue;

            var tr = document.createElement("tr");

            var nombre = document.createElement("td");
            var a = document.createElement("a");
            // Pasar la ID del artículo a insertarDatoCarrito al hacer clic en el artículo.
            a.addEventListener("click", function() { carrito.insertarDatoCarrito(this.id); });
            a.href = "#";
            a.id = datos[i].id;
            a.title = datos[i].nombre;
            a.appendChild(document.createTextNode(datos[i].nombre))
            nombre.appendChild(a);

            var unidades = document.createElement("td");
            unidades.appendChild(document.createTextNode(datos[i].unidades + " uds"));

            var precio = document.createElement("td");
            precio.appendChild(document.createTextNode(datos[i].precio + " €"));
            
            var cantidad = document.createElement("td");
            var input = document.createElement("input");
            input.onkeyup = function(e) {
                if (e.keyCode == 13)
                    document.getElementById("sumar" + this.id.substr(8)).click();
            };
            input.id = "cantidad" + datos[i].id;
            input.style.width = "50px";
            input.type = "text";
            cantidad.appendChild(input);
            
            var botonSuma = document.createElement("td");
            var sumar = document.createElement("input");
            sumar.addEventListener("click", function() { carrito.insertarMultiples(this.id); });
            sumar.id = "sumar" + datos[i].id;
            sumar.type = "button";
            sumar.value = "+";
            botonSuma.appendChild(sumar);

            tr.appendChild(nombre);
            tr.appendChild(unidades);
            tr.appendChild(precio);
            tr.appendChild(cantidad);
            tr.appendChild(botonSuma);
            
            articulosTabla.appendChild(tr);
        }
    }

    // Privado
    function construirCategorias(datos)
    {
        var listaCategorias = document.getElementById("categoriasLista");

        // Vaciar la lista de categorías
        listaCategorias.innerHTML = "";

        var vistos = [];

        for (var i = 0; i < datos.length; i++)
        {    
            if (vistos.indexOf(datos[i].cat) != -1)
                continue;

            var texto = "";

            switch (datos[i].cat)
            {
                case 1:
                    texto = "Música";
                    break;

                case 2:
                    texto = "Ropa";
                    break;

                default:
                    texto = "Desconocida";
                    break;
            }

            var a = document.createElement("a");
            a.addEventListener("click", function() { construirArticulos(datos, this.id); });
            a.href = "#";
            a.id = datos[i].cat;
            a.title = texto;
            a.appendChild(document.createTextNode(texto))

            var li = document.createElement("li");
            li.appendChild(a);

            listaCategorias.appendChild(li);

            vistos.push(datos[i].cat);
        }    
    }
    
    function recargarTienda()
    {
        localStorage.removeItem("tienda");
        carrito.vaciarCarrito();
        obtenerDisponibilidad();
        estadoTienda.innerHTML = "Tienda recargada";
    }
    
    this.recargarTienda = recargarTienda;
}
